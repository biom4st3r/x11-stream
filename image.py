from PIL import Image

with open('image', 'rb') as file:
    data = file.read()

print(len(data)/(1920*2*1080))
image = Image.new('RGB', (1920 * 2, 1080))
i = 0
for y in range(1080):
    for x in range(1920 * 2):
        datai = i * 4
        image.putpixel((x, y), (data[datai + 2], data[datai + 1], data[datai + 0]))
        i += 1
image.save('image.png')
