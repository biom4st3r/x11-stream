
pub const SPEED_TESTING = false;
pub const SPEED_TESTING_DEBUG = true;

// disables sampling uv
pub const FAST_YUV422 = true;

pub const DUMMY_FILE = false;

pub const YUV422_MAX_THREADS = 8;