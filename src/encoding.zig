pub const av = @cImport({ // https://github.com/FFmpeg/FFmpeg/blob/n3.0/doc/examples/decoding_encoding.c
    @cInclude("libavcodec/avcodec.h");
    @cInclude("libavutil/opt.h");
    @cInclude("libavutil/channel_layout.h");
    @cInclude("libavutil/common.h");
    @cInclude("libavutil/imgutils.h");
    @cInclude("libavutil/mathematics.h");
    @cInclude("libavutil/samplefmt.h");
});
const flags = @import("flags.zig");
const std = @import("std");
const print = std.log.err;
const Profiler = @import("tools.zig").Profiler;
const RGBA = @import("tools.zig").RGBA;



// 31fps
pub fn yuv444AndCopy(image: [*]u8, width: usize, height: usize, bytesperpixel: usize, frame: *av.AVFrame, usefloat: bool) void {
    var i: usize = 0;
    while (i < width * height) : (i += 1) {
        var img_i = i * bytesperpixel;
        if (usefloat) {
            const T: type = f32; //f16 is 1/2 the speed
            const b: T = @intToFloat(T, image[img_i + 0]);
            const g: T = @intToFloat(T, image[img_i + 1]);
            const r: T = @intToFloat(T, image[img_i + 2]);
            const Y =  0.257 * r + 0.504 * g + 0.098 * b + 16;
            const U = -0.148 * r - 0.291 * g + 0.439 * b + 128;
            const V =  0.439 * r - 0.368 * g - 0.071 * b + 128;
            frame.data[0][i] = @floatToInt(u8, Y);
            frame.data[1][i] = @floatToInt(u8, U);
            frame.data[2][i] = @floatToInt(u8, V);
        } else {
            // Get the rgb from the screenshot
            const T: type = i16;
            const b: T = @intCast(T, image[img_i + 0]);
            const g: T = @intCast(T, image[img_i + 1]);
            const r: T = @intCast(T, image[img_i + 2]);
            // convert to YUV
            const Y = 16 + (((r<<6)+(r<<1)+(g<<7)+g+(b<<4)+(b<<3)+b) >> 8);
            const U = 128 + ((-((r<<5)+(r<<2)+(r<<1)) - ( (g<<6)+(g<<3)+(g<<1)) + ( b<<7) - ( b<<4)) >> 8);
            const V = 128 + (((r<<7)-(r<<4)-((g<<6)+(g<<5)-(g<<1))-((b<<4)+(b<<1))) >> 8);
            // write the pixels to AVFrame
            frame.data[0][i] = @intCast(u8, Y & 0b1111_1111);
            frame.data[1][i] = @intCast(u8, U & 0b1111_1111);
            frame.data[2][i] = @intCast(u8, V & 0b1111_1111);
        }
    }
}



pub fn yuv422AndCopy(image: [*]u8, width: usize, height: usize, bytesperpixel: usize, frame: *av.AVFrame) void {
    var i: usize = 0;
    // print("{} {} {}", .{width, height, bytesperpixel});
    // var pixels: [*]RGBA = @ptrCast([*]RGBA, @alignCast(4, image));
    _ = bytesperpixel;
 
    const pixels: [*]RGBA = @ptrCast([*]RGBA, @alignCast(4, image));
    while (i < width * height) : (i += 1) {
        const T: type = i32;
        const p = pixels[i].cast(T);
        // convert to YUV using magic
        const Y = 16 + (((p.r<<6)+(p.r<<1)+(p.g<<7)+p.g+(p.b<<4)+(p.b<<3)+p.b) >> 8);
        // write the pixels to AVFrame
        frame.data[0][i] = @intCast(u8, Y & 0b1111_1111);
        if (i & 1 == 0) {
            const U = 128 + ((-((p.r<<5)+(p.r<<2)+(p.r<<1)) - ((p.g<<6)+(p.g<<3)+(p.g<<1)) + (p.b<<7) - (p.b<<4)) >> 8);
            const V = 128 + (((p.r<<7)-(p.r<<4)-((p.g<<6)+(p.g<<5)-(p.g<<1))-((p.b<<4)+(p.b<<1))) >> 8);
            // the u and v buffers are half size so, half the index
            frame.data[1][i >> 1] = @intCast(u8, U & 0b1111_1111);
            frame.data[2][i >> 1] = @intCast(u8, V & 0b1111_1111);
        } else if (!comptime(flags.FAST_YUV422)) {
            // properly sample else nearest neighbor
            const U = 128 + ((-((p.r<<5)+(p.r<<2)+(p.r<<1)) - ((p.g<<6)+(p.g<<3)+(p.g<<1)) + (p.b<<7) - (p.b<<4)) >> 8);
            const V = 128 + (((p.r<<7)-(p.r<<4)-((p.g<<6)+(p.g<<5)-(p.g<<1))-((p.b<<4)+(p.b<<1))) >> 8);
            frame.data[1][i >> 1] = @intCast(u8, ((U + frame.data[1][i >> 1]) >> 1) & 0b1111_1111); 
            frame.data[2][i >> 1] = @intCast(u8, ((V + frame.data[2][i >> 1]) >> 1) & 0b1111_1111);
        }
    }
}

fn yuv422AndCopy_t_body(start: usize, max: usize, bytesperpixel: usize, image: [*]u8, frame: *av.AVFrame) void {
    var i: usize = start;
    while (i < max) : (i += 1) {
        const T: type = i32;
        const img_i = i * bytesperpixel;
        // const p = pixels[i].cast(T);
        const b: T = image[img_i + 0];
        const g: T = image[img_i + 1];
        const r: T = image[img_i + 2];
        
        // convert to YUV using magic
        const Y = 16 + (((r<<6)+(r<<1)+(g<<7)+g+(b<<4)+(b<<3)+b) >> 8);
        // write the pixels to AVFrame
        frame.data[0][i] = @intCast(u8, Y & 0b1111_1111);
        if (i & 1 == 0) { // 42fps
            const U = 128 + ((-((r<<5)+(r<<2)+(r<<1)) - ((g<<6)+(g<<3)+(g<<1)) + (b<<7) - (b<<4)) >> 8);
            const V = 128 + (((r<<7)-(r<<4)-((g<<6)+(g<<5)-(g<<1))-((b<<4)+(b<<1))) >> 8);
            // the u and v buffers are half size so, half the index
            frame.data[1][i >> 1] = @intCast(u8, U & 0b1111_1111);
            frame.data[2][i >> 1] = @intCast(u8, V & 0b1111_1111);
        } else if (!comptime(flags.FAST_YUV422)) { // 31fps
            // properly sample else nearest neighbor
            const U = 128 + ((-((r<<5)+(r<<2)+(r<<1)) - ((g<<6)+(g<<3)+(g<<1)) + (b<<7) - (b<<4)) >> 8);
            const V = 128 + (((r<<7)-(r<<4)-((g<<6)+(g<<5)-(g<<1))-((b<<4)+(b<<1))) >> 8);
            frame.data[1][i >> 1] = @intCast(u8, ((U + frame.data[1][i >> 1]) >> 1) & 0b1111_1111); 
            frame.data[2][i >> 1] = @intCast(u8, ((V + frame.data[2][i >> 1]) >> 1) & 0b1111_1111);
        }
    }
}

const MAX_THREADS = flags.YUV422_MAX_THREADS;
const SHIFT = std.math.log2_int(usize, MAX_THREADS);

const RT: type = if (flags.SPEED_TESTING_DEBUG) [2]u64 else void;
// Want faster? Use a compute shader or yuv420
pub fn yuv422AndCopy_t(image: [*]u8, width: usize, height: usize, bytesperpixel: usize, frame: *av.AVFrame) RT {
    _ = bytesperpixel;
    var time = std.time.Timer.start() catch unreachable;
    var data: RT = if (RT == void) undefined else [_]u64{0,0};
    const segsize = (height * width) >> SHIFT;

    var spawns: [MAX_THREADS]std.Thread = undefined;
    if (RT != void) time.reset();
    for (spawns) |_,i| {
        const start: usize = segsize * (i);
        const max: usize = segsize * (i+1);
        spawns[i] = std.Thread.spawn(.{.stack_size = 16 * 1024 * 1024}, yuv422AndCopy_t_body, .{start, max, bytesperpixel, image, frame}) catch unreachable;
    }
    if (RT != void) data[0] = time.lap();
    if (RT != void) time.reset();
    for (spawns) |*thread| {
        thread.join();
    }
    if (RT != void) data[1] = time.lap();
    if (RT != void) return data;
}

pub fn yuv422AndCopy_async(image: [*]u8, width: usize, height: usize, bytesperpixel: usize, frame: *av.AVFrame) RT {
    _ = bytesperpixel;
    var time = std.time.Timer.start() catch unreachable;
    var data: RT = if (RT == void) undefined else [_]u64{0,0};
    const segsize = (height * width) >> SHIFT;
    const pixels: [*]RGBA = @ptrCast([*]RGBA, @alignCast(4, image));
    var frames: [MAX_THREADS]@Frame(yuv422AndCopy_t_body) = undefined;

    if (RT != void) time.reset();
    for (frames) |_,i| {
        const start: usize = segsize * i;
        const max: usize = segsize * (i + 1);
        frames[i] = async yuv422AndCopy_t_body(start, max, pixels, frame);
    }
    if (RT != void) data[0] = time.lap();
    if (RT != void) time.reset();
    for (frames) |*f| {
        await f;
    }
    if (RT != void) data[1] = time.lap();
    if (RT != void) return data;
}