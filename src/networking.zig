
// https://datatracker.ietf.org/doc/html/rfc3550#section-5
const RTP_HEADER = packed struct {

    pub fn create(list_size: u4) RTP_HEADER {

    }
};

pub fn RTP_HEADER(csrc_list_size: u4, marker: u1) type {
    return packed struct {
        // version of RTP
        version: u2 = 0b10, // v2
        // If the padding bit is set, the packet contains one or more
        //   additional padding octets at the end which are not part of the
        //   payload.  The last octet of the padding contains a count of how
        //   many padding octets should be ignored, including itself.  Padding
        //   may be needed by some encryption algorithms with fixed block sizes
        //   or for carrying several RTP packets in a lower-layer protocol data
        //   unit.
        padding_0: u1,
        // If the extension bit is set, the fixed header MUST be followed by
        //   exactly one header extension, with a format defined in Section
        //   5.3.1.
        extension: u1,
        // The CSRC count contains the number of CSRC identifiers that follow
        //   the fixed header.
        // ContribA: uting source (CSRC) source of a stream of RTP packets
        //   that has contributed to the combined stream produced by an RTP
        //   mixer (see below).
        CSRC: u4,
        // The interpretation of the marker is defined by a profile.  It is
        //   intended to allow significant events such as frame boundaries to
        //   be marked in the packet stream.  A profile MAY define additional
        //   marker bits or specify that there is no marker bit by changing the
        //   number of bits in the payload type field (see Section 5.3).
        marker: u1 = 0,
        // This field identifies the format of the RTP payload and determines
        //   its interpretation by the application.  A profile MAY specify a
        //   default static mapping of payload type codes to payload formats.
        payload_type: u7,
        // The sequence number increments by one for each RTP data packet
        //   sent, and may be used by the receiver to detect packet loss and to
        //   restore packet sequence.
        sequence_number: u16,
        // The timestamp reflects the sampling instant of the first octet in
        //   the RTP data packet.  The sampling instant MUST be derived from a
        //   clock that increments monotonically and linearly in time to allow
        //   synchronization and jitter calculations
        // RFC-6184: A 90 kHz clock rate MUST be used.
        timestamp: u32,
        // The SSRC field identifies the synchronization source.  This
        //   identifier SHOULD be chosen randomly, with the intent that no two
        //   synchronization sources within the same RTP session will have the
        //   same SSRC identifier.
        SSRC: u32,
        CSRC_LIST: [@intCast(usize, csrc_list_size)]u32,
    };
}