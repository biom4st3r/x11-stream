const std = @import("std");
const xlib = @cImport({
    @cInclude("X11/Xlib.h");
    // @cInclude("sys/shm.h");
    // @cInclude("X11/extensions/XShm.h");
    @cInclude("X11/X.h");
    @cInclude("X11/Xutil.h");
    @cInclude("X11/Xlibint.h");
    @cInclude("X11/Xproto.h");
});
const encoding = @import("encoding.zig");
const flags = @import("flags.zig");
const assert = std.debug.assert;
const print = std.log.err;
const Profiler = @import("tools.zig").Profiler;

fn saveImage(image: *xlib.XImage) anyerror!void {
    const file = try std.fs.cwd().createFile("image.raw", .{.read=true, .truncate=true});
    defer file.close();
    
    const bytesppix = @intCast(usize, image.bits_per_pixel) / 8;
    print("bytesppix: {}", .{bytesppix});

    const MAX = @intCast(usize, image.width) * @intCast(usize, image.height) * bytesppix;
    try file.writeAll(image.data[0..MAX]);
}

pub fn makeScreenshot() anyerror!void {
    var ctx = XLibCtx.create();
    const image: *xlib.XImage = ctx.createScreenshot();
    defer _ = image.f.destroy_image.?(image);
    print("depth{}", .{image.depth});
    try saveImage(image);
}

const AVCtx = struct {
    codec: *const encoding.av.AVCodec,
    ctx: *encoding.av.AVCodecContext,
    pkt: *encoding.av.AVPacket,
    fps: usize,
    frame_time: u64,
    // frame: *encoding.av.AVFrame,
    pub fn createFrame(self: *AVCtx) EError!*encoding.av.AVFrame {
        var frame: ?*encoding.av.AVFrame = encoding.av.av_frame_alloc();
        frame.?.format = self.ctx.pix_fmt;
        frame.?.width = self.ctx.width;
        frame.?.height = self.ctx.height;
        var ret = encoding.av.av_frame_get_buffer(frame, 0);
        if (ret < 0) return EError.FAILED_ALLOC_FRAME_BUFFER;
        if (frame == null) return EError.BAD_FRAME;
        return frame.?;
    }

    pub fn freeFrame(frame: *encoding.av.AVFrame) void {
        encoding.av.av_free(frame);
    }

    pub fn free(self: *AVCtx) void {
        // encoding.av.av_free(self.codec);
        encoding.av.av_free(self.ctx);
        encoding.av.av_free(self.pkt);
    }

    pub fn create(codec_id: c_uint, pix_fmt: c_int, width: c_int, height: c_int, bitrate: i64, fps: c_int) anyerror!AVCtx {
        // https://github.com/FFmpeg/FFmpeg/blob/master/doc/examples/encode_video.c
        const codec: ?*const encoding.av.AVCodec = encoding.av.avcodec_find_encoder(codec_id); // AV_CODEC_ID_H264
        if (codec == null) return EError.BAD_CODEC;
        var ctx: ?*encoding.av.AVCodecContext = encoding.av.avcodec_alloc_context3(codec);
        if (ctx == null) return EError.BAD_CTX;
        var pkt: ?*encoding.av.AVPacket = encoding.av.av_packet_alloc();
        ctx.?.bit_rate = bitrate;
        ctx.?.width = width;
        ctx.?.height = height;
        ctx.?.time_base = encoding.av.AVRational{ .num=1, .den=fps};
        ctx.?.framerate = encoding.av.AVRational{ .num=fps, .den=1};
        ctx.?.gop_size = 10;
        ctx.?.max_b_frames = 1;
        ctx.?.pix_fmt = pix_fmt;
        if (codec.?.id == encoding.av.AV_CODEC_ID_H264) {
            _ = encoding.av.av_opt_set(ctx.?.priv_data, "preset", "slow", 0);
        }
        var ret: c_int = encoding.av.avcodec_open2(ctx, codec, 0);
        if (ret < 0) return EError.FAILED_TO_OPEN_CODEC;
        return AVCtx {
            .codec = codec.?,
            .ctx = ctx.?,
            .pkt = pkt.?,
            .fps = @intCast(usize, fps),
            .frame_time = @floatToInt(u64, 1.0/@intToFloat(f32, fps) * 1_000_000_000),
        };
    }
};

// Explore openGL for screenshoting
pub const XLibCtx = struct {
    display: ?*xlib.Display = undefined,
    root: xlib.Window = undefined,
    width: c_uint = undefined,
    height: c_uint = undefined,
    attr: xlib.XWindowAttributes = undefined,
    x: c_int,
    y: c_int,

    pub fn create() XLibCtx {
        const _display: ?*xlib.Display = xlib.XOpenDisplay(0).?;
        const _root: xlib.Window = xlib.XDefaultRootWindow(_display);
        var _gwa: xlib.XWindowAttributes = undefined;

        _ = xlib.XGetWindowAttributes(_display, _root, &_gwa);
        return XLibCtx {
            .display = _display,
            .root = _root,
            .width = @intCast(c_uint, _gwa.width),
            .height = @intCast(c_uint, _gwa.height),
            .x = 0,
            .y = 0,
            .attr = _gwa,
        };
    }
    
    pub fn createImage(self: *XLibCtx, buffer: [*]u8) *xlib.XImage {
        return xlib.XCreateImage(self.display, self.attr.visual, 24, xlib.ZPixmap, 0, buffer, self.width, self.height, 32, 0);
    }

    pub fn _create_buffer(self: *XLibCtx, alloc: std.mem.Allocator, bytesperpixel: usize) []u8 {
        return alloc.alloc(u8, self.width * self.height * bytesperpixel) catch unreachable;
    }

    // pub fn xshm() void {
    //     var shminfo: xlib.XShmSegmentInfo = undefined;
    //     var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    //     var alloc = gpa.allocator();

    //     // print("{}", .{@intCast(usize, image.width) * @intCast(usize, image.height) * bytesppix == 1080*1920*2*4});
    //     var array: []u8 = alloc.alloc(u8, @intCast(usize, image.width) * @intCast(usize, image.height) * 4) catch unreachable;
    //     defer alloc.free(array);
    //     image = xlib.XShmCreateImage(xlibctx.display, xlibctx.attr.visual, 24, X.ZPixmap, @ptrCast([*]u8, array), &shminfo, xlibctx.width, xlibctx.height);
    //     shminfo.shmid = xlib.shmget(xlib.IPC_PRIVATE, @intCast(usize, image.bytes_per_line * image.height), xlib.IPC_CREAT|0777);
    //     shminfo.shmaddr = image.data; // = xlib.shmat(xlib.shminfo.shmid, 0, 0);
    //     shminfo.readOnly = 0;
    //     _ = xlib.XShmAttach(xlibctx.display, &shminfo);
    // }

    pub fn createScreenshot(self: *XLibCtx) *xlib.XImage {
        return xlib.XGetImage(
            self.display, self.root, self.x, self.y, self.width, self.height, 0x00FF_FFFF, xlib.ZPixmap
        );
    }

    pub fn fast_updateScreenshot(self: *XLibCtx, image: *xlib.XImage) *xlib.XImage {
        var rep: xlib.xGetImageReply = undefined;
        var req: *xlib.xGetImageReq = undefined;
        // https://github.com/changbiao/libav/blob/master/libavdevice/x11grab.c
        // https://github.com/mirror/libX11/blob/5faa8dc0b364f55f19034c370436074646d9ad54/src/GetImage.c

        self.display.?.lock_fns[0].lock_display.?(self.display);

        defer {
            self.display.?.lock_fns[0].unlock_display.?(self.display); // xlib.UnlockDisplay(self.display);
            if (self.display.?.synchandler) |handle| {
                _ = handle(self.display);
            } // xlib.SyncHandle();
        }
        req = @ptrCast(*xlib.xGetImageReq, @alignCast(4, xlib._XGetRequest(self.display, xlib.X_GetImage, @sizeOf(xlib.xGetImageReq)).?));
        req.drawable = @intCast(c_uint, self.root);
        req.x = @intCast(c_short, self.x);
        req.y =  @intCast(c_short, self.y);
        req.width =  @intCast(c_ushort, self.width);
        req.height = @intCast(c_ushort, self.height);
        req.planeMask = @intCast(c_uint, xlib.AllPlanes & 0xFFFF_FFFF);
        req.format = @intCast(u8, xlib.ZPixmap);
        var ret = xlib._XReply(self.display, @ptrCast(*xlib.xReply, &rep), 0, xlib.xFalse);
        if (ret == 0) {
            print("HALP T_T", .{});
            return image;
        }

        xlib._XReadPad(self.display, image.data, @intCast(c_long, rep.length) << 2); // FIX ME MAGIC NUMBER
        return image;
    }

    pub fn updateScreenshot(self: *XLibCtx, image: *xlib.XImage) *xlib.XImage {
        return xlib.XGetSubImage(
            self.display, self.root, self.x, self.y, self.width, self.height, xlib.AllPlanes, xlib.ZPixmap, image, 0, 0 // this should probably be x,y
        );
    }
};

const EError = error {
    BAD_CODEC,
    FAILED_TO_OPEN_CODEC,
    BAD_CTX,
    BAD_PACKET,
    BAD_FRAME,
    FAILED_ALLOC_FRAME_BUFFER,
    FAILED_FRAME_WRITABLE,
    FAILED_SEND_FRAME,
    FAILED_ENCODING,
};

pub fn delay_image(xlibctx: *XLibCtx, frame_time: u64, wait_timer: *std.time.Timer) *xlib.XImage {
    // GOOD ENOUGH
    if (!comptime(flags.SPEED_TESTING)) { // Allow testing speed improvements
        defer wait_timer.reset();
        if (wait_timer.read() <= frame_time) {
            // wait for the time between frames
            std.time.sleep((frame_time - wait_timer.read()));
        } else {
            print("dropping frames {} > {}", .{wait_timer.read(), frame_time});
        }
    } 
    return xlibctx.createScreenshot();
}

pub fn delay_image_update(xlibctx: *XLibCtx, frame_time: u64, wait_timer: *std.time.Timer, image: *xlib.XImage) *xlib.XImage {
    // GOOD ENOUGH
    if (!comptime(flags.SPEED_TESTING)) { // Allow testing speed improvements
        defer wait_timer.reset();
        const time = wait_timer.read();
        if (time <= frame_time) {
            // wait for the time between frames
            std.time.sleep(frame_time - time);
        } else {
            print("dropping frames {} > {}", .{wait_timer.read(), frame_time});
        }
    } 
    return xlibctx.fast_updateScreenshot(image);
}

/// Returns total frames created
pub fn videoEncode(avctx: *AVCtx, xlibctx: *XLibCtx, duration: usize) anyerror!void {
    // var score: Scoreboard = if (flags.SPEED_TESTING_DEBUG) Scoreboard.create() else undefined;
    // var score: if (flags.SPEED_TESTING_DEBUG) Scoreboard else void = if (flags.SPEED_TESTING_DEBUG) Scoreboard{} else undefined;
    var profiler = Profiler(
        6,
        "\ncreate_ss: {d:.3}ms\nrgba->yuv: {d:.3}ms\nencode_frame: {d:.3}ms\nfree_image: {d:.3}ms\nthread_creation: {d:.3}ms\nthread_runtime: {d:.3}ms\nsum: {d:.3}/{d:.3}ms\nfps: {d:.3}").init();
    const RES = 1_000_000; // ms
    
    var frame: *encoding.av.AVFrame = try avctx.createFrame();
    defer AVCtx.freeFrame(frame);

    var file = try std.fs.cwd().createFile(comptime(if (flags.DUMMY_FILE) "/dev/null" else "video.h264"), std.fs.File.CreateFlags{.read=true, .truncate=true});
    defer file.close();

    var ret = encoding.av.av_frame_make_writable(frame);
    if (ret < 0) return EError.FAILED_FRAME_WRITABLE;

    var i: usize = 0;

    // create XImage struct with my buffer
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var alloc = gpa.allocator();
    var buffer: []u8 = xlibctx._create_buffer(alloc, 4);
    defer alloc.free(buffer);
    var image: *xlib.XImage = xlibctx.createImage(@ptrCast([*]u8, buffer));    // BGRA 
    defer {
        image.data = null;
        _ = image.f.destroy_image.?(image);
    }

    // waiter for frame waiting
    var wait_timer = try std.time.Timer.start();
    while (i < (avctx.fps * duration)) : (i += 1) {
        defer {
            profiler.inc();
            if (i & 31 == 0) {
                // print("{}", .{profiler.sum()});
                const total_time = (profiler.sum() - profiler.times[4] - profiler.times[5]) / profiler.count;
                profiler.stats_reset(.{
                    profiler.times[0] / profiler.count,   // create_ss
                    profiler.times[1] / profiler.count,   // yuv
                    profiler.times[2] / profiler.count,   // encode frame
                    profiler.times[3] / profiler.count,   // free_image
                    profiler.times[4] / profiler.count,   // create
                    profiler.times[5] / profiler.count,   // wait
                    total_time,      // total ms
                    1.0 / @intToFloat(f32, avctx.fps) * 1_000,
                    1 / (total_time/1000),
                });
            }
        }

        profiler.res();
        // update screenshot
        image = delay_image_update(xlibctx, avctx.frame_time, &wait_timer, image);
        profiler.add_time(0, RES);

        const bytesppix = @intCast(usize, image.bits_per_pixel) / 8;
        profiler.res();
        // convert bgra->yuv422 and copy into libav frame
        var out = encoding.yuv422AndCopy_t(image.data, @intCast(usize, image.width), @intCast(usize, image.height), bytesppix, frame);
        if (comptime(flags.SPEED_TESTING_DEBUG)) {
            profiler.times[4] += @intToFloat(f32, out[0]) / RES;
            profiler.times[5] += @intToFloat(f32, out[1]) / RES;
        } else {_ = out;}
        profiler.add_time(1, RES);


        profiler.res();
        frame.pts = @intCast(i64, i);
        try frame_encode(avctx.ctx, frame, avctx.pkt, &file);
        profiler.add_time(2, RES);
    }
    try frame_encode(avctx.ctx, frame, avctx.pkt, &file);
    _ = try file.write(&[_]u8{ 0, 0, 1, 0xb7 }); // magic?
}

pub fn frame_encode(ctx: *encoding.av.AVCodecContext, frame: ?*encoding.av.AVFrame, pkt: *encoding.av.AVPacket, file: *std.fs.File) anyerror!void {
    var ret: c_int = encoding.av.avcodec_send_frame(ctx, frame);
    if (ret < 0) return EError.FAILED_SEND_FRAME;

    ret = encoding.av.avcodec_receive_packet(ctx, pkt);
    while (ret >= 0) : (ret = encoding.av.avcodec_receive_packet(ctx, pkt)) {
        if (ret == encoding.av.AVERROR(encoding.av.EAGAIN) or ret == encoding.av.AVERROR_EOF) {
            return;
        } else if (ret < 0) {
            return EError.FAILED_ENCODING;
        }

        _ = try file.write(pkt.data[0..@intCast(usize, pkt.size)]);
        
        encoding.av.av_packet_unref(pkt);
    }
}

const ARGS_ERROR = 
    \\--trans-threads=x     number of thread to use for transcoding rgba->yuv422
    \\--duration=x          length of video
    \\--fast_yuv            sacrefic color correctness for speed
    \\--outfile=            output file name {outfile}.h264
  ;
pub fn main() anyerror!void {
    var xlibctx = XLibCtx.create();
    xlibctx.width = 1920;
    var avctx: AVCtx = try AVCtx.create(
        encoding.av.AV_CODEC_ID_H264, 
        encoding.av.AV_PIX_FMT_YUV422P, 
        @intCast(c_int, xlibctx.width), 
        @intCast(c_int, xlibctx.height), 
        10_000_000,
        100, // fps
    );
    defer avctx.free();

    try videoEncode(&avctx, &xlibctx, 15);

}
