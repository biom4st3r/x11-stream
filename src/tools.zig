const std = @import("std");
const print = std.log.err;
const flags = @import("flags.zig");

pub fn Profiler(comptime size: usize, comptime fmt_str: []const u8) type {
    return struct {
        const Self = @This();
        const SIZE = size;
        const fmt_str: []const u8 = fmt_str;
        count: f32 = 0,
        times: [SIZE]f32 = undefined,
        TIMER: std.time.Timer,

        pub fn init() Self {
            return .{
                .TIMER = std.time.Timer.start() catch unreachable
            };
        }

        pub fn sum(self: *Self) f32 {
            var f: f32 = 0;
            for (self.times) |time| {
                f += time;
            }
            return f;
        }

        pub fn stats(self: *Self, args: anytype) void {
            _ = self;
            if (!comptime(flags.SPEED_TESTING_DEBUG)) return;
            print(fmt_str, args);
        }

        pub fn add_time(self: *Self, index: usize, RES: f32) void {
            if (!comptime(flags.SPEED_TESTING_DEBUG)) return;
            self.times[index] += @intToFloat(f32, self.TIMER.lap()) / RES;
        }

        pub fn stats_reset(self: *Self, args: anytype) void {
            if (!comptime(flags.SPEED_TESTING_DEBUG)) return;
            self.stats(args);
            for (self.times) |_,i| {
                self.times[i] = 0;
            }
            self.count = 0;
        }

        pub fn inc(self: *Self) void {
            if (!comptime(flags.SPEED_TESTING_DEBUG)) return;
            self.count += 1;
        }

        pub fn res(self: *Self) void {
            if (!comptime(flags.SPEED_TESTING_DEBUG)) return;
            self.TIMER.reset();
        }
    };
}

const RGBA = packed struct {
    b: u8,
    g: u8,
    r: u8,
    a: u8,
    pub fn cast(self: *RGBA, comptime T: type) struct {b: T, g: T, r: T, a: T} {
        return .{
            .b=@intCast(T, self.b),
            .g=@intCast(T, self.g),
            .r=@intCast(T, self.r),
            .a=@intCast(T, self.a),
        };
    }
};


const RGB = packed struct {
    b: u8,
    g: u8,
    r: u8,
    pub fn cast(self: *RGB, comptime T: type) struct {b: T, g: T, r: T} {
        return .{
            .b=@intCast(T, self.b),
            .g=@intCast(T, self.g),
            .r=@intCast(T, self.r),
        };
    }
};